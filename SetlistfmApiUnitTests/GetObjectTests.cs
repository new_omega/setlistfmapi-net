﻿using System;
using SetlistfmAPI;
using SetlistfmAPI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SetlistfmApiUnitTests
{
    [TestClass]
    public class GetObjectTests
    {
        static string apiToken = "";
        SetlistApi controller = new SetlistApi(apiToken, "en");

        [TestMethod]
        public void SetlistApiArtist()
        {
            string mbid = "67f66c07-6e61-4026-ade5-7e782fad3a5d";

            Artist result = controller.Artist(mbid);

            Assert.IsNotNull(result, "Artist() returned null.");

            Assert.AreEqual<string>(mbid, result.MBID, "Artists(), property MBID is not equal.");
        }

        [TestMethod]
        public void SetlistApiCity()
        {
            int geoId = 5392171;

            City result = controller.City(geoId);

            Assert.IsNotNull(result, "City() returned null.");
            Assert.IsNotNull(result.Coords, "City(), property Coords is null.");
            Assert.IsNotNull(result.Country, "City(), property Country is null.");

            Assert.AreEqual<int>(geoId, result.Id, "City(), property Id is not equal.");
        }

        [TestMethod]
        public void SetlistApiSetlist()
        {
            string setlistId = "53d6ab39";

            Setlist result = controller.Setlist(setlistId);

            Assert.IsNotNull(result, "Setlist() returned null.");
            Assert.IsNotNull(result.Artist, "Setlist(), property Artist is null.");
            Assert.IsNotNull(result.Venue, "Setlist(), property Venue is null.");
            Assert.IsNotNull(result.Venue.City, "Setlist(), property Venue.City is null.");
            Assert.IsNotNull(result.Venue.City.Coords, "Setlist(), property Venue.City.Coords is null.");
            Assert.IsNotNull(result.Venue.City.Country, "Setlist(), property Venue.City.Country is null.");
            Assert.AreEqual<string>(setlistId, result.Id);
        }

        [TestMethod]
        public void SetlistApiUser()
        {
            string userId = "New_Omega";

            User result = controller.User(userId);

            Assert.IsNotNull(result, "User() returned null.");
            Assert.AreEqual<string>(userId, result.UserId, "User(), property Id is not equal.");
        }

        [TestMethod]
        public void SetlistApiVenue()
        {
            string venueId = "3bd46418";

            Venue result = controller.Venue(venueId);

            Assert.IsNotNull(result, "Venue() returned null.");
            Assert.IsNotNull(result.City, "Venue(), property City is null.");
            Assert.IsNotNull(result.City.Coords, "Venue(), property City.Coords is null.");
            Assert.IsNotNull(result.City.Country, "Venue(), property City.Country is null.");

            Assert.AreEqual<string>(venueId, result.Id, "Venue(), property Id is not equal.");            
        }
    }
}
